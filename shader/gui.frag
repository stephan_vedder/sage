#version 330 core

in vec2 vecUV;
in vec4 vecColor;

uniform sampler2D textureSampler;

out vec4 color;

void main()
{
	if(texture( textureSampler, vecUV ) == vec4(0, 0, 0, 1))
    	color = vecColor;
	else
		color = texture( textureSampler, vecUV ) * vecColor;
}

