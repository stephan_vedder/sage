#version 330 core

in vec3 position;
in vec2 UV;
in vec4 color;

uniform mat4 mvp;

out vec2 vecUV;
out vec4 vecColor;

void main()
{
	gl_Position = mvp * vec4(position, 1.0f);
	vecUV = UV;
	vecColor = color;
}
