#version 330 core

in vec3 pos;
in vec3 normal;
in vec3 color;

out vec3 inColor;
out vec3 inNormal;

// Values that stay constant for the whole mesh.
uniform mat4 MVP;

void main()
{	
	// Output position of the vertex, in clip space : MVP * position
	gl_Position =  MVP * vec4(pos,1.0f);

	// The color of each vertex will be interpolated
	// to produce the color of each fragment
	inColor = color;

	// Calculate the normal vector against the world matrix only.
	inNormal = mat3(MVP) * normal;

	// Normalize the normal vector.
	inNormal = normalize(inNormal);
}
