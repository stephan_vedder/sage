#version 330 core

in vec3 pos;
in vec2 UV;
in vec4 col;

uniform vec2 translation;
uniform mat4 ortho;

out vec2 vecUV;
out vec4 vecColor;

void main()
{	
	vec3 tran_pos = vec3(pos + vec3(translation, 0.0f));
	gl_Position = ortho * vec4(tran_pos, 1.0f);
	vecUV = UV;
	vecColor = col;
}
