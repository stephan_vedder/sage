#version 330 core

in vec3 inNormal;

//ambient lightning
uniform vec3 ambientColor;
uniform vec3 diffuseColor;

uniform vec3 lightDir;

// Ouput data
out vec3 outColor;

void main()
{
        // Invert the light direction for calculations.
        vec3 tempDir = -lightDir;

        // Calculate the amount of light on this pixel.
        float lightIntensity = clamp(dot(inNormal, tempDir), 0.0f, 1.0f);

        // Determine the final amount of diffuse color based on the diffuse color combined with the light intensity.
        vec3 diffuse =  clamp((diffuseColor * lightIntensity), 0.0f, 1.0f);

        // Multiply the texture pixel and the final diffuse color to get the final pixel color result.
        outColor = max(diffuse, ambientColor);
}