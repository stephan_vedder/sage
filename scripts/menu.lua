-- 2015 (C) SAGE Engine
-- Patryk Szczygło

function onStart()
	Game.registerEventToElement("menu", "start-menu", "mouseover", "mouseoverSound");
	Game.registerEventToElement("menu", "options-menu", "mouseover", "mouseoverSound");
	Game.registerEventToElement("menu", "credits-menu", "mouseover", "mouseoverSound");
	Game.registerEventToElement("menu", "exit-menu", "mouseover", "mouseoverSound");

	Game.registerEventToElement("menu", "start-menu", "click", "goToSelectMap");
	Game.registerEventToElement("menu", "exit-menu", "click", "exitGame");
	
	Game.loadDocument("selectmap.rml");
	Game.hideDocument("selectmap");
	Game.loadDocument("game.rml");
	Game.hideDocument("game");
	Game.showDocument("menu");
end

function mouseoverSound()
	Game.playSound("SFX_MOUSEOVER", "data/sounds/sound1.ogg");
end

function goToSelectMap()
	Game.hideDocument("menu");
	Game.showDocument("selectmap");
end

function exitGame()
	Game.exit()
end

onStart();
