-- 2015 (C) SAGE Engine
-- Patryk Szczygło

function onStart()
	files = "";
	for k,v in ipairs( Game.listFiles("./maps/") ) do
		if string.find(v, ".map", 1, true) then
			Game.addChild("selectmap", "maps_list", "map_button", 
					"<button id=\"" .. v .."\">" .. tostring(v) .. "</button>");
			Game.registerEventToElement("selectmap", v, "click", "goToGame");
		end
	end

	Game.registerEventToElement("selectmap", "back", "click", "goToMenu");
	Game.registerEventToElement("selectmap", "back", "mouseover", "mouseoverSound");
end

function mouseoverSound()
	Game.playSound("SFX_MOUSEOVER", "data/sounds/sound1.ogg");
end

function goToMenu()
	Game.hideDocument("selectmap");
	Game.showDocument("menu");
end

function goToGame()
	Game.loadMap(Game.clicked_element_id);
	Game.hideDocument("selectmap");
	Game.showDocument("game");
end


onStart();
