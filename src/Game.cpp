// 2014 (C) SAGE Engine
// Stephan Vedder
#include "Game.hpp"
#include "Config.hpp"
#include "Map.hpp"
#include "Script/ScriptMngr.hpp"
#include "Script/ScriptHelper.hpp"
#include "GUI/EventManager.hpp"
#include "GUI/GuiManager.hpp"
#include "Graphics/GraphicsMngr.hpp"
#include "./Util/Logger.hpp"
#include "Util/FPS.hpp"

Game::Game() : m_current_map(nullptr)
{
	sf::VideoMode resolution(Config::WIDTH, Config::HEIGHT, Config::COLOR);
	sf::ContextSettings settings(Config::DEPTH, Config::STENCIL, Config::ALIASING, Config::GL_MAJOR, Config::GL_MINOR);
	m_window.create(resolution, "SAGE", sf::Style::Default, settings);
	if (!m_window.isOpen())
		SAGELogCrash("Could not open SFML Window");

	settings = m_window.getSettings();
	if (settings.majorVersion == Config::GL_MAJOR)
		if(settings.minorVersion < Config::GL_MINOR)
			SAGELogCrash("Could not initialize GL 3.3 Context or higher");
	else if(settings.majorVersion < Config::GL_MAJOR)
		SAGELogCrash("Could not initialize GL 3.3 Context or higher");

	m_graphics = std::make_unique<Graphics::Manager>();
	m_script = std::make_unique<Script::Manager>();
	m_gui = std::make_unique<GUI::Manager>(m_window);
	
	Script::Helper::init(this);
	GUI::EventManager::init(m_script.get());
	
	// setting menu
	m_gui->LoadDocument("menu.rml", m_script.get());
}

void Game::Run()
{
	int update_fps = 0;

	while (m_window.isOpen())
	{
		m_graphics->Clear();

		sf::Event event;
		while (m_window.pollEvent(event))
		{
			m_gui->Update(event);		
		}

		updateCamera();

		Util::FPS::update();		
		
		if (update_fps > 1000)
		{
			auto fps = m_gui->getContext().GetFocusElement()->GetElementById("fps_counter");
			if (fps)
				fps->SetInnerRML(std::to_string(Util::FPS::getFPS()).c_str());

			update_fps -= 1000;
		}
		if(m_current_map != nullptr)
		{
			m_graphics->Render(m_current_map->GetTerrain());
		}
		m_gui->Render();
		m_window.display();
		++update_fps;
	}
}

Game::~Game()
{
}

void Game::loadCurrentMap(const std::string& filepath)
{
	m_current_map = std::make_unique<Graphics::Map>();
	m_current_map->Load(filepath);
	m_current_map->Create();
}

void Game::updateCamera()
{
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		Graphics::Camera& cam = m_graphics->GetCamera();
		glm::vec3 dir = cam.GetDirection();
		dir = glm::rotate(dir, -0.001f, glm::vec3(0.0f, 1.0f, 0.0f));
		cam.SetDirection(dir);
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		Graphics::Camera& cam = m_graphics->GetCamera();
		glm::vec3 dir = cam.GetDirection();
		dir = glm::rotate(dir, 0.001f, glm::vec3(0.0f, 1.0f, 0.0f));
		cam.SetDirection(dir);
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		Graphics::Camera& cam = m_graphics->GetCamera();
		glm::vec3 dir = cam.GetDirection();
		dir = glm::rotate(dir, -0.001f, glm::vec3(1.0f, 0.0f, 0.0f));
		cam.SetDirection(dir);
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		Graphics::Camera& cam = m_graphics->GetCamera();
		glm::vec3 dir = cam.GetDirection();
		dir = glm::rotate(dir, -0.001f, glm::vec3(-1.0f, 0.0f, 0.0f));
		cam.SetDirection(dir);
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{
		Graphics::Camera& cam = m_graphics->GetCamera();
		cam.SetPosition(cam.GetPosition() - 0.01f*glm::cross(cam.GetDirection(), glm::vec3(0, 1, 0)));
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		Graphics::Camera& cam = m_graphics->GetCamera();
		cam.SetPosition(cam.GetPosition() + 0.01f*glm::cross(cam.GetDirection(), glm::vec3(0, 1, 0)));
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
	{
		Graphics::Camera& cam = m_graphics->GetCamera();
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::LShift))
			cam.SetPosition(cam.GetPosition() + glm::vec3(0.0f, -0.01f, 0.0f));
		else
			cam.SetPosition(cam.GetPosition() + glm::vec3(0.0f, 0.01f, 0.0f));
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		Graphics::Camera& cam = m_graphics->GetCamera();
		cam.SetPosition(cam.GetPosition() + 0.01f*cam.GetDirection());
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
	{
		Graphics::Camera& cam = m_graphics->GetCamera();
		cam.SetPosition(cam.GetPosition() - 0.01f*cam.GetDirection());
	}
}
