// 2014 (C) SAGE Engine
// Stephan Vedder
#pragma once
#include <stdint.h>
#include <string>
class Config
{
public:
    static uint16_t WIDTH,HEIGHT;
    static uint8_t GL_MAJOR, GL_MINOR;
    static uint8_t DEPTH, STENCIL, COLOR;
    static uint8_t ALIASING;
	static std::string GAME_NAME;
	static std::string GUI_DIR;
	static std::string SCRIPT_DIR;
	static std::string SHADER_DIR;
	static std::string MAP_DIR;
};