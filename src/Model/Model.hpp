// 2015 (C) SAGE Engine
// Patryk Szczygło

#pragma once

#include "ModelFile.hpp"
#include "../Graphics/Shader.hpp"
#include <vector>

namespace Model
{
	class ModelData
	{
	public:
		struct Mesh
		{
			MeshHeader header;
			
			glm::vec3* vertices; 
			glm::vec3* normals;
			
			Triangle* triangles;
			
			MaterialInfo material_info;
			std::vector<Material> materials;
			std::vector<Texture> textures;
			Box bounding_box;
		};
		
		ModelData();
		
		void addMesh(Mesh* mesh);
		inline std::vector<Mesh*> getMeshes() { return m_meshes; }
		
		void render(const glm::mat4& mvp);
		
	private:
		std::vector<Mesh*> m_meshes;
		Graphics::Shader m_shader;
	};
}
