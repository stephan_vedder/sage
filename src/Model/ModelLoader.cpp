// 2015 (C) SAGE Engine
// Patryk Szczygło

#include "ModelLoader.hpp"
#include "../Util/Logger.hpp"
#include <sstream>

using namespace Model;

// util to convert int to hex, used to log error
std::string intToHexString(int num)
{
	std::stringstream stream;
	stream << std::hex << num;
	return stream.str();
}

ModelData* Loader::loadFromFile(const std::string& filepath)
{
	std::ifstream fin(filepath, std::ios::binary);
	if(fin.fail())
		return nullptr;
		
	uint32 filesize = getFileSize(fin);
	ModelData* model = new ModelData();

	while((uint32)fin.tellg() < filesize)
	{
		uint32 chunktype  = read<uint32>(fin);
		uint32 curr_chunksize = getChunkSize(read<uint32>(fin));

		switch(chunktype)
		{
			case CHUNK_MESH:
				model->addMesh(loadMeshChunk(fin, curr_chunksize));
			break;
			case CHUNK_HIERARCHY:
				fin.seekg(curr_chunksize, std::ios::cur);
			break;
			case CHUNK_ANIMATION:
				fin.seekg(curr_chunksize, std::ios::cur);
			break;
			case CHUNK_BOX:
				fin.seekg(curr_chunksize, std::ios::cur);
			break;
			default:
				std::cout << "(loadFromFile)Wrong chunk 0x" << intToHexString(chunktype) << std::endl;
				fin.seekg(curr_chunksize, std::ios::cur);
			break;
		}
	}
	fin.close();
	return model;
}

uint32 Loader::getFileSize(std::ifstream& fin)
{
	fin.seekg(0, std::ios::end);
	uint32 size = (uint32)fin.tellg();
	fin.seekg(0, std::ios::beg);
	return size;
}

uint32 Loader::getChunkSize(uint32 data)
{
	return data & 0x7FFFFFFF;
}

Material Loader::readMaterial(std::ifstream& fin, uint32 chunksize)
{
	Material material;
	uint32 chunkend = (uint32)fin.tellg() + chunksize;
	
	while(fin.tellg() < chunkend)
	{
		uint32 chunktype = read<uint32>(fin);
		uint32 curr_chunksize = getChunkSize(read<uint32>(fin));
		
		switch(chunktype)
		{
			case CHUNK_MATERIAL_NAME:
				material.name = readString(fin);
			break;
			case CHUNK_VERTEX_MATERIAL_INFO:
				material.vertex_material_info = read<VertexMaterialInfo>(fin);
			break;
			default:
				std::cout << "(readMaterial)Wrong chunk 0x" << intToHexString(chunktype) << std::endl;
				fin.seekg(curr_chunksize, std::ios::cur);
			break;
		}
	}
	
	return material;
}

Texture Loader::readTexture(std::ifstream& fin, uint32 chunksize)
{
	Texture texture;
	uint32 chunkend = (uint32)fin.tellg() + chunksize;
	
	while(fin.tellg() < chunkend)
	{
		uint32 chunktype = read<uint32>(fin);
		uint32 curr_chunksize = getChunkSize(read<uint32>(fin));
		
		switch(chunktype)
		{
			case CHUNK_TEXTURE_NAME:
				texture.name = readString(fin);
			break;
			case CHUNK_TEXTURE_INFO:
				texture.texture_info = read<TextureInfo>(fin);
			break;
			default:
				std::cout << "(readTexture)Wrong chunk 0x" << intToHexString(chunktype) << std::endl;
				fin.seekg(curr_chunksize, std::ios::cur);
			break;
		}
	}
	
	return texture;
}


ModelData::Mesh* Loader::loadMeshChunk(std::ifstream& fin, uint32 chunksize)
{
	auto chunkend = (uint32)fin.tellg() + chunksize;
	ModelData::Mesh* mesh = new ModelData::Mesh();

	while(fin.tellg() < chunkend)
	{
		uint32 chunktype = read<uint32>(fin);
		uint32 curr_chunksize = getChunkSize(read<uint32>(fin));
		
		switch (chunktype)
		{
			case CHUNK_VERTICIES:
				readArray(fin, curr_chunksize, mesh->vertices);
			break;
			case CHUNK_VERTEX_NORMALS:
				readArray(fin, curr_chunksize, mesh->normals);
			break;
			case CHUNK_MESH_HEADER:
				mesh->header = read<MeshHeader>(fin);
			break;
			case CHUNK_TRIANGLES:
				readArray(fin, curr_chunksize, mesh->triangles);
			break;
			case CHUNK_MATERIAL_INFO:
				mesh->material_info = read<MaterialInfo>(fin);
			break;
			case CHUNK_VERTEX_MATERIALS:
				mesh->materials = loadMaterials(fin, curr_chunksize);
			break;
			case CHUNK_TEXTURES:
				mesh->textures = loadTextures(fin, curr_chunksize);
			break;
			default:
				std::cout << "(loadMeshChunk)Wrong chunk 0x" << intToHexString(chunktype) << std::endl;
				fin.seekg(curr_chunksize, std::ios::cur);
			break;
		}
	}

	return mesh;
}

std::vector<Material> Loader::loadMaterials(std::ifstream& fin, uint32 chunksize)
{
	uint32 chunkend = (uint32)fin.tellg() + chunksize;
	std::vector<Material> materials;

	while (fin.tellg() < chunkend)
	{
		uint32 chunktype = read<uint32>(fin);
		uint32 curr_chunksize = getChunkSize(read<uint32>(fin));

		switch (chunktype)
		{
			case CHUNK_VERTEX_MATERIAL:
				materials.push_back(readMaterial(fin, curr_chunksize));
			break;
			default:
				std::cout << "(loadMaterials)Wrong chunk 0x" << intToHexString(chunktype) << std::endl;
				fin.seekg(curr_chunksize, std::ios::cur);
			break;
		}
	}

	return materials;
}

std::vector<Texture> Loader::loadTextures(std::ifstream& fin, uint32 chunksize)
{
	uint32 chunkend = (uint32)fin.tellg() + chunksize;
	std::vector<Texture> textures;

	while (fin.tellg() < chunkend)
	{
		uint32 chunktype = read<uint32>(fin);
		uint32 curr_chunksize = getChunkSize(read<uint32>(fin));

		switch (chunktype)
		{
			case CHUNK_TEXTURE:
				textures.push_back(readTexture(fin, curr_chunksize));
			break;
			default:
				std::cout << "(loadTextures)Wrong chunk 0x" << intToHexString(chunktype) << std::endl;
				fin.seekg(curr_chunksize, std::ios::cur);
			break;
		}
	}

	return textures;
}
