// 2015 (C) SAGE Engine
// Patryk Szczygło

#include "Model.hpp"
#include <iostream>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

using namespace Model;

class RendererGeometryHandler
{
	public:
		GLuint vbo, vao;
		int num_vertices;
		
		RendererGeometryHandler() : vbo(0), vao(0), num_vertices(0) 
		{}
		
		~RendererGeometryHandler()
		{
			if(vbo)
				glDeleteBuffers(1, &vbo);
			if(vao)
				glDeleteBuffers(1, &vao);
			
			vbo = 0;
			vao = 0;
		}
};

struct ModelRendererVertex
{
	glm::vec3 position;
	glm::vec2 tex_coord;
	glm::vec4 color;
};

ModelData::ModelData()
{
	m_shader.LoadFromFile(Graphics::Shader::VERTEX_SHADER, "model.vert");
	m_shader.LoadFromFile(Graphics::Shader::FRAGMENT_SHADER, "model.frag");
	m_shader.AddAttribute("position");
	m_shader.AddAttribute("UV");
	m_shader.AddAttribute("color");
	m_shader.Link();
	m_shader.AddUniform("mvp");
}

void ModelData::addMesh(Mesh* mesh)
{
	m_meshes.push_back(mesh); 
}

void ModelData::render(const glm::mat4& mvp)
{
	std::vector<ModelRendererVertex> data;
	std::vector<int> indices;
	
	int num_vertices = 0;
	int num_indices = 0;
	
	for(auto mesh : getMeshes())
	{
		num_vertices += mesh->header.num_vertices;
		num_indices += mesh->header.num_triangles * 3; // multiply by 3 vertex of triangle
		
		for(unsigned int i = 0; i < mesh->header.num_triangles; i++)
		{
			indices.push_back(mesh->triangles[i].vertex_index[0]);
			indices.push_back(mesh->triangles[i].vertex_index[1]);
			indices.push_back(mesh->triangles[i].vertex_index[2]);
		}
		
		for(unsigned int i = 0; i < mesh->header.num_vertices; i++)
		{
			ModelRendererVertex vertex;
			vertex.position = mesh->vertices[i];
			// TODO: add texture and color rendering
			vertex.tex_coord = glm::vec2(0,0);
			vertex.color = glm::vec4(1,0,0,1);
			data.push_back(vertex);
		}
	}
	
	RendererGeometryHandler* geometry = new RendererGeometryHandler();
	geometry->num_vertices = num_vertices;
	
	glGenBuffers(1, &geometry->vbo);
	glBindBuffer(GL_ARRAY_BUFFER, geometry->vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(ModelRendererVertex) * num_vertices, &data[0],
		GL_STATIC_DRAW);

	glGenBuffers(1, &geometry->vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, geometry->vao);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int) * num_indices, &indices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	m_shader.Use();
	glBindBuffer(GL_ARRAY_BUFFER, geometry->vbo);
	
	// position
	glEnableVertexAttribArray(m_shader.Attribute("position"));
	glVertexAttribPointer(m_shader.Attribute("position"), 3, GL_FLOAT,
		GL_FALSE, sizeof(ModelRendererVertex), (void*)offsetof(ModelRendererVertex, position));
	
	// UV
	glEnableVertexAttribArray(m_shader.Attribute("UV"));
	glVertexAttribPointer(m_shader.Attribute("UV"), 2, GL_FLOAT,
		GL_FALSE, sizeof(ModelRendererVertex), (void*)offsetof(ModelRendererVertex, tex_coord));
	
	// Colors
	glEnableVertexAttribArray(m_shader.Attribute("color"));
	glVertexAttribPointer(m_shader.Attribute("color"), 4, GL_FLOAT,
		GL_FALSE, sizeof(ModelRendererVertex), (void*)offsetof(ModelRendererVertex, color));
	
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, geometry->vao);
	
	glUniformMatrix4fv(m_shader.Uniform("mvp"), 1, GL_FALSE, glm::value_ptr(mvp));
	
	glDrawElements(GL_TRIANGLES, num_indices, GL_UNSIGNED_INT, 0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDisableVertexAttribArray(m_shader.Attribute("position"));
	glDisableVertexAttribArray(m_shader.Attribute("UV"));
	glDisableVertexAttribArray(m_shader.Attribute("color"));
	m_shader.UnUse();
	
	delete geometry;
}
