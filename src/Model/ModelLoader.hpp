// 2015 (C) SAGE Engine
// Patryk Szczygło

#pragma once 

#include "Model.hpp"
#include "ModelFile.hpp"
#include <string>
#include <fstream>
#include <memory>
#include <iostream>

namespace Model
{
	class Loader
	{
	public:
		static ModelData* loadFromFile(const std::string& filepath);
		
	private:
		static uint32 getFileSize(std::ifstream& fin);
		static uint32 getChunkSize(uint32 data);
		
		// read any fixed-size data type
		template <typename T>
		static inline const T read(std::ifstream& fin)
		{
			T result = T();
			fin.read(reinterpret_cast<char*>(&result), sizeof(T));
			return result;
		}
		
		template<typename T>
		static inline void readArray(std::ifstream& fin, uint32 chunksize, T*& arr)
		{
			auto num_elements = chunksize / sizeof(T);
			arr = new T[num_elements];
			fin.read((char*)arr, sizeof(T)*num_elements);
		}
		
		static inline std::string readString(std::ifstream& fin)
		{
			std::string buffer;
			char c;
			while ((c = fin.get()) != '\0') 
			{
				buffer += c;
			}
			return buffer;
		}
		
		static Material readMaterial(std::ifstream& fin, uint32 chunksize);
		static Texture readTexture(std::ifstream& fin, uint32 chunksize);
		
		static ModelData::Mesh* loadMeshChunk(std::ifstream& fin, uint32 chunksize);
		static std::vector<Material> loadMaterials(std::ifstream& fin, uint32 chunksize);
		static std::vector<Texture> loadTextures(std::ifstream& fin, uint32 chunksize);
	};
}
