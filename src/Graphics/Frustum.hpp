// 2014 (C) SAGE Engine
// Stephan Vedder
#pragma once
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <array>
#include "Camera.hpp"

namespace Graphics
{
	class Frustum
	{
	public:
		inline std::array<std::array<float, 4>, 6> GetFrustumArray()
		{
			m_updated = false;
			return m_frustum;
		}

		// Returns true if Frustum changed since last GetFrustumArray call. Should be sufficient for now.
		inline bool Updated()
		{
			return m_updated;
		}

		void Recalculate(Graphics::Camera& cam);
	private:
		glm::mat4 m_lastView;
		glm::mat4 m_lastProjection;

		bool m_updated; 
		std::array<std::array<float, 4>, 6> m_frustum; // 6 planes, each described by point and normal in this order: NEAR, FAR, TOP, BOTTOM, LEFT, RIGHT 
	};
}