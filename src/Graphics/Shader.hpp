// 2014 (C) SAGE Engine
// Stephan Vedder
#pragma once
#include "flextGL.h"
#include <array>
#include <map>

namespace Graphics
{
	class Shader
	{
	public:
		Shader();
		~Shader();

		enum Type
		{
			VERTEX_SHADER = 0,
			CONTROL_SHADER = 1,
			EVALUATION_SHADER = 2,
			GEOMETRY_SHADER = 3,
			FRAGMENT_SHADER = 4
		};

		void LoadFromFile(const Type type, const std::string& filename);
		void Link();

		void Use() const;
		void UnUse() const;

		void AddUniform(const std::string& uniform);
		void AddAttribute(const std::string& attribute);

		GLuint Uniform(const std::string& uniform);
		GLuint Attribute(const std::string& attribute);
	private:
		void LoadFromMemory(const Type type, const std::string& content);

		GLuint m_program;
		std::array<GLuint, 5> m_shaders;
		std::map<std::string, GLint> m_attributeList;
		std::map<std::string, GLint> m_uniformLocationList;
	};
}
