// 2014 (C) SAGE Engine
// Stephan Vedder

#include "Camera.hpp"

using namespace Graphics;

Camera::Camera()
{
	m_pos = glm::vec3(0.0f, 1.0f, 0.0f);
	m_up = glm::vec3(0.0f, -1.0f, 0.0f);
	m_direction = glm::vec3(1.0f, 0.0f, 1.0f);
	m_ratio = 4.0f / 3.0f;

	m_fov = 75.0f;
	m_clip_near = 1.0f;
	m_clip_far = 10000.0f;

	m_model = glm::mat4(1.0f);
	m_view = glm::lookAt(m_pos, m_pos + m_direction, m_up);
	m_projection = glm::perspective(m_fov, m_ratio, m_clip_near, m_clip_far);
	m_mvp = m_projection* m_view * m_model;
}