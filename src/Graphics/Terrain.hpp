// 2014 (C) SAGE Engine
// Stephan Vedder
#pragma once
#include <SFML/Graphics.hpp>
#include <stdint.h>
#include <glm/glm.hpp>
#include <memory>
#include "Shader.hpp"
#include "Quadtree.hpp"
#include "Frustum.hpp"

namespace Graphics
{
	class Terrain
	{
	public:
		Terrain();
		~Terrain();

		void Create(const glm::ivec2& size);
		void Render(const glm::mat4& mvp, Frustum& frustum);

		inline void SetHeightmap(const std::shared_ptr<sf::Image>& heightmap)
		{
			m_heightmap = heightmap;
		}
		
		inline void SetAmbientColor(const glm::vec3& ambient)
		{
			m_ambient = ambient;
		}

		inline void SetDiffuseColor(const glm::vec3& diffuse)
		{
			m_diffuse = diffuse;
		}

		inline void SetLightDir(const glm::vec3& dir)
		{
			m_lightDir = dir;
		}

	private:
		struct Vertex
		{
			glm::vec3 pos;
			glm::vec3 normal;
		};

		void CalculateNormals(std::vector<Vertex>& vertices);
	private:
		GLuint m_vao;

		GLuint m_vbo;
		GLuint m_ibo;

		uint32_t m_indicecount;
		uint32_t m_vertexcount;
		Shader m_shader;
		std::shared_ptr<sf::Image> m_heightmap;
		glm::vec3 m_ambient;
		glm::vec3 m_diffuse;
		glm::vec3 m_lightDir;
		glm::ivec2 m_size;

		std::shared_ptr<Quadtree> m_quadtree;

	};
}
