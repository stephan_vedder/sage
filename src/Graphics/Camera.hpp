// 2014 (C) SAGE Engine
// Stephan Vedder
#pragma once
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>

namespace Graphics
{
	class Camera
	{
	public:
		Camera();

		inline void SetFOV(const double& fov)
		{
			m_fov = fov;
			m_projection = glm::perspective(m_fov, m_ratio, m_clip_near, m_clip_far);
		}

		inline void SetClipping(const double& clip_near,const double& clip_far)
		{
			m_clip_near = clip_near;
			m_clip_far = clip_far;
			m_projection = glm::perspective(m_fov, m_ratio, m_clip_near, m_clip_far);
		}

		inline void SetRatio(const double& ratio) 
		{
			m_ratio = ratio;
			m_projection = glm::perspective(m_fov, m_ratio, m_clip_near, m_clip_far);
		}

		inline void SetPosition(const glm::vec3& pos)
		{
			m_pos = pos;
		}

		inline void SetDirection(const glm::vec3& direction)
		{
			m_direction = direction;
		}

		inline void Update()
		{
			m_view = glm::lookAt(m_pos, m_pos+m_direction, m_up);
			m_mvp = m_projection* m_view * m_model;
		}

		inline const glm::mat4& GetMVP()
		{
			return m_mvp;
		}

		inline const glm::mat4& GetProjection()
		{
			return m_projection;
		}

		inline const glm::mat4& GetView()
		{
			return m_view;
		}

		inline glm::vec3& GetPosition()
		{
			return m_pos;
		}

		inline glm::vec3& GetDirection()
		{
			return m_direction;
		}
	private:
		glm::vec3 m_pos;
		glm::vec3 m_up;
		glm::vec3 m_direction;

		glm::mat4 m_model;
		glm::mat4 m_view;
		glm::mat4 m_projection;
		glm::mat4 m_mvp;

		double m_fov;
		//clipping
		double m_clip_near;
		double m_clip_far;
		//aspect ratio
		double m_ratio;
	};
}