// 2014 (C) SAGE Engine
// Stephan Vedder
#pragma once 
#include <vector>
#include <memory>
#include <SFML/Graphics.hpp>
#include "Shader.hpp"
#include "Terrain.hpp"
#include "Frustum.hpp"
#include "Camera.hpp"

namespace Graphics
{

	class Manager
	{
	public:
		Manager();
		~Manager();


		inline void Clear()
		{
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		}
		void Render(Terrain&);
		inline Camera& GetCamera()
		{
			return m_cam;
		}		
	private:
		Camera m_cam;
		Frustum m_frustum;
	};

}
