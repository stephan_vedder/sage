// 2014 (C) SAGE Engine
// Stephan Vedder
#include "flextGL.h"
#include "../Util/Logger.hpp"
#include "GraphicsMngr.hpp"

using namespace Graphics;

Manager::Manager()
{
    if (flextInit() != GL_TRUE)
        SAGELogCrash("Failed to initialize flext!");

	//activate GL_ARB_DEBUG
	if (FLEXT_ARB_debug_output)
	{
		glDebugMessageCallbackARB(&Util::GLDebug, NULL);
	}
	
	m_frustum.Recalculate(m_cam);
}

Manager::~Manager()
{

}

void Manager::Render(Terrain& terrain)
{
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glDepthFunc(GL_LESS);
	m_cam.Update();
	m_frustum.Recalculate(m_cam);
	auto mvp = m_cam.GetMVP();
	terrain.Render(mvp,m_frustum);
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
}
