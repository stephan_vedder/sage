// 2014 (C) SAGE Engine
// Stephan Vedder
#pragma once
#include <glm/glm.hpp>
#include <vector>
#include <stdint.h>
#include "Shader.hpp"

namespace Graphics
{
	struct Particle
	{
		glm::vec3 pos;
		glm::vec3 velocity;
		glm::int32 lifetime;
		glm::int32 remaining;
	};

	//TODO: make this an entity
	class Emitter
	{
	public:
		struct EmitterInfo
		{
			uint32_t interval;
			uint32_t lifetime;
			uint32_t particle_lifetime;
			glm::vec3 color;
			glm::vec3 velocity;
		};
	public:
		Emitter(const EmitterInfo& info);
		~Emitter();

		void Render(const glm::mat4& mvp);
	private:
		std::vector<Particle> m_particles;

	};

};