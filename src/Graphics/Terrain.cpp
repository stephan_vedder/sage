// 2014 (C) SAGE Engine
// Stephan Vedder
#include "Terrain.hpp"
#include "../Util/Logger.hpp"
#include <vector>
#include <SFML/Graphics.hpp>
#include <glm/gtc/type_ptr.hpp>
using namespace Graphics;

Terrain::Terrain() : m_vbo(0), m_ibo(0)
{
	glGenVertexArrays(1, &m_vao);
	glBindVertexArray(m_vao);

	glGenBuffers(1, &m_vbo);
	glGenBuffers(1, &m_ibo);

	m_ambient = glm::vec3(0.1f,0.1f,0.1f);
	m_diffuse = glm::vec3(0.8f,0.8f,0.8f);
	m_lightDir = glm::vec3(0.0f, -1.0f, -1.0f);

	m_shader.LoadFromFile(Shader::VERTEX_SHADER, "terrain.vert");
	m_shader.LoadFromFile(Shader::FRAGMENT_SHADER, "terrain.frag");
	m_shader.AddAttribute("pos");
	m_shader.AddAttribute("normal");
	m_shader.Link();

	m_shader.AddUniform("MVP");
	m_shader.AddUniform("ambientColor");
	m_shader.AddUniform("diffuseColor");
	m_shader.AddUniform("lightDir");
}

Terrain::~Terrain()
{
	glDeleteVertexArrays(1, &m_vao);
	glDeleteBuffers(1, &m_vbo);
	glDeleteBuffers(1, &m_ibo);
}

void Terrain::Render(const glm::mat4& mvp, Graphics::Frustum& frustum)
{
	// If frustum didnt change - no need to recalculate visible triangles.
	if (frustum.Updated())
	{ 
		std::vector<uint16_t> indices = m_quadtree->GetTriangles(frustum.GetFrustumArray()); // Should be called only when frustum has changed.
		m_indicecount = indices.size();
		if (m_indicecount!=0)
		{ 
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_indicecount * sizeof(uint16_t), &indices[0], GL_DYNAMIC_DRAW);
		}
	}
	m_shader.Use();
	
	//glBindVertexArray(m_vao);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glEnableVertexAttribArray(m_shader.Attribute("pos"));
	glEnableVertexAttribArray(m_shader.Attribute("normal"));

	glVertexAttribPointer(m_shader.Attribute("pos"), 3, GL_FLOAT,
		GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, pos));

	glVertexAttribPointer(m_shader.Attribute("normal"), 3, GL_FLOAT,
		GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, normal));
		
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);

	glUniformMatrix4fv(m_shader.Uniform("MVP"), 1, false, glm::value_ptr(mvp));
	glUniform3fv(m_shader.Uniform("ambientColor"),1,glm::value_ptr(m_ambient));
	glUniform3fv(m_shader.Uniform("diffuseColor"), 1, glm::value_ptr(m_diffuse));
	glUniform3fv(m_shader.Uniform("lightDir"), 1, glm::value_ptr(m_lightDir));

	glDrawElements(GL_TRIANGLES,  m_indicecount,
				GL_UNSIGNED_SHORT,nullptr);

	glDisableVertexAttribArray(m_shader.Attribute("pos"));
	glDisableVertexAttribArray(m_shader.Attribute("normal"));
	m_shader.UnUse();
	//glBindVertexArray(0);
}

void Graphics::Terrain::CalculateNormals(std::vector<Vertex>& vertices)
{
	int index1, index2, index3, index, count;
	glm::vec3 vertex1, vertex2, vertex3, vector1, vector2, sum, length;
	glm::vec3* normals = new glm::vec3[(m_size.y - 1) * (m_size.x - 1)];
	
	for (auto j = 0; j<(m_size.y - 1); j++)
	{
		for (auto i = 0; i<(m_size.x - 1); i++)
		{
			index1 = (j * m_size.x) + i;
			index2 = (j * m_size.x) + (i + 1);
			index3 = ((j + 1) * m_size.x) + i;

			vertex1 = vertices[index1].pos;
			vertex2 = vertices[index2].pos;
			vertex3 = vertices[index3].pos;

			vector1 = vertex1 - vertex3;
			vector2 = vertex3 - vertex2;

			index = (j * (m_size.x - 1)) + i;

			normals[index] = glm::cross(vector1, vector2);
		}
	}

	for (auto j = 0; j<m_size.x; j++)
	{
		for (auto i = 0; i<m_size.y; i++)
		{
			sum = glm::vec3(0.0f);
			count = 0;

			if (((i - 1) >= 0) && ((j - 1) >= 0))
			{
				index = ((j - 1) * (m_size.x - 1)) + (i - 1);

				sum += normals[index];
				count++;
			}

			if ((i < (m_size.x - 1)) && ((j - 1) >= 0))
			{
				index = ((j - 1) * (m_size.y - 1)) + i;

				sum += normals[index];
				count++;
			}

			if (((i - 1) >= 0) && (j < (m_size.y - 1)))
			{
				index = (j * (m_size.x - 1)) + (i - 1);

				sum += normals[index];
				count++;
			}

			if ((i < (m_size.x - 1)) && (j < (m_size.y - 1)))
			{
				index = (j * (m_size.x - 1)) + i;

				sum += normals[index];
				count++;
			}

			sum /= (float)count;

			index = (j * m_size.y) + i;
			vertices[index].normal = glm::normalize(sum);
		}
	}

	// Release the temporary normals.
	delete[] normals;
	normals = 0;
}

void Terrain::Create(const glm::ivec2& size) 
{
	if (size.x != m_heightmap->getSize().x ||
		size.y != m_heightmap->getSize().y)
	{
		SAGELogCrash("Heightmap size doesn't fit terrain size");
	}

	m_size = size;

	m_quadtree = std::make_shared<Quadtree>(glm::vec2(size.x/2.f,size.y/2.f),glm::vec2(size.x/2.f,size.y/2.f),5); // Consider saving Quadtree level in map file.

	//glBindVertexArray(m_vao);
	//Create vertices first
	std::vector<Vertex> vertices;

	for (auto y = 0; y < size.y; ++y)
	{
		for (auto x = 0; x < size.x; ++x)
		{
			auto offset = y*size.x + x;
			Vertex vert;
			auto height = m_heightmap->getPixel(x, y).r/ 20.0f;
			vert.pos = glm::vec3(x, height, y);
			vertices.push_back(vert);
		}
	}

	CalculateNormals(vertices);

	m_vertexcount = vertices.size();
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBufferData(GL_ARRAY_BUFFER, m_vertexcount * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

	std::vector<uint16_t> indices;
	for (auto y = 0; y < size.y-1; ++y)
	{
		for (auto x = 0; x < size.x-1; ++x)
		{
			//lower left triangle
			indices.push_back((y + 1)*size.x + x);			
			indices.push_back(y*size.x + x+1);
			indices.push_back(y*size.x + x);
			//upper right triangle
			indices.push_back((y + 1)*size.x + x);
			indices.push_back((y + 1)*size.x + x+1);
			indices.push_back(y*size.x + x + 1);
		}
	}
	
	for (int i = 0; i < indices.size(); i+=3)
	{
		m_quadtree->AddTriangle(&indices[i], vertices[indices[i]].pos, vertices[indices[i+1]].pos, vertices[indices[i+2]].pos);
	}
}
