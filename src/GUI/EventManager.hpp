// 2015 (C) SAGE Engine
// Patryk Szczygło

#pragma once

#include <Rocket/Core/EventListener.h>
#include <Rocket/Core/Types.h>
#include <SFML/Graphics.hpp>
#include <string>
#include <vector>
#include <memory>
#include "../Script/ScriptMngr.hpp"

namespace GUI
{
	struct CustomEvent
	{
		CustomEvent(std::string eve, std::string elem, std::string func) :
			event(eve), element(elem), function(func) {}
		std::string event;
		std::string element;
		std::string function;
	};
	
	class EventManager : public Rocket::Core::EventListener
	{
	public:
		static void init(Script::Manager* script);
		static void registerEvent(Rocket::Core::Element* element, const std::string& event_name, const std::string& func_name);
	private:
		virtual void ProcessEvent(Rocket::Core::Event& event);
		
		static std::vector<std::unique_ptr<CustomEvent>> m_events;
		
		static Script::Manager* m_script;
	};
}
