// 2015 (C) SAGE Engine
// Patryk Szczygło

#include <iostream>
#include "EventManager.hpp"
#include <Rocket/Core/Element.h>
#include "../AssetsManager/AssetsManager.hpp"
#include "../Util/Logger.hpp"

using namespace GUI;

static EventManager event_listener;

Script::Manager* EventManager::m_script;
std::vector<std::unique_ptr<CustomEvent>> EventManager::m_events;


void EventManager::init(Script::Manager* script)
{
	m_script = script;
}

void EventManager::registerEvent(Rocket::Core::Element* element, const std::string& event_name, const std::string& func_name)
{
	element->AddEventListener(event_name.c_str(), &event_listener);	
	m_events.push_back(std::make_unique<CustomEvent>(event_name, element->GetId().CString(), func_name));
}

void EventManager::ProcessEvent(Rocket::Core::Event& event)
{
	lua_getglobal(m_script->getLuaState(), "Game");
	int game = lua_gettop(m_script->getLuaState());
	
	if(lua_isnil(m_script->getLuaState(), game))
		SAGELogCrash("Error getting lua table in C++: failed to get \"Game\" class.");
	
	
	for(const auto& e: m_events)
	{
		if(event == e->event.c_str())
		{
			std::string event_id = event.GetCurrentElement()->GetId().CString();
			if(event_id == e->element.c_str())
			{
				if(event == "click")
				{
					lua_pushstring(m_script->getLuaState(), event_id.c_str());
					lua_setfield(m_script->getLuaState(), game, "clicked_element_id");
				}
				m_script->runFunction(e->function.c_str());
			}
		}
	}
	
	lua_pop(m_script->getLuaState(), 1); // pop game
}
