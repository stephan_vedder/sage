// 2015 (C) SAGE Engine
// Stephan Veddery
// Patryk Szczygło

#include "GuiManager.hpp"
#include "../Config.hpp"
#include "../Util/Logger.hpp"
#include <Rocket/Debugger.h>
#include <iostream>

using namespace GUI;
using namespace Rocket;

Manager::Manager(sf::RenderWindow& window) : m_window(nullptr)
{
	m_renderer.SetWindow(&window);
	Core::SetRenderInterface(&m_renderer);
	Core::SetSystemInterface(&m_system);
	Core::Initialise();
	m_context = Core::CreateContext("sage", Core::Vector2i(Config::WIDTH, Config::HEIGHT));
	m_window = &window;

	Rocket::Debugger::Initialise(m_context);
}

Manager::~Manager()
{
	m_context->RemoveReference();
	Core::Shutdown();
}

Rocket::Core::ElementDocument* Manager::LoadDocument(const std::string & filename, Script::Manager* script)
{
	auto doc = m_context->LoadDocument((Config::GUI_DIR+filename).c_str());
	if (doc == nullptr)
		SAGELogCrash("Could not load gui document: " + filename);
		
	doc->Show();

	auto script_elem = doc->GetElementById("script");
	if(script_elem != NULL)
	{
		auto attr = script_elem->GetAttribute("src");
		if(attr != NULL)
		{
			Rocket::Core::String script_name;
			if(attr->GetInto<Rocket::Core::String>(script_name))
				script->runScript(script_name.CString());
		}
	}
	
	doc->RemoveReference();
	m_documents.push_back(doc);
	return m_documents.back();
}

void Manager::Update(sf::Event& event)
{
	switch (event.type)
	{
	case sf::Event::Resized:
		m_renderer.Resize();
		break;
	case sf::Event::MouseMoved:
		m_context->ProcessMouseMove(event.mouseMove.x, event.mouseMove.y,
			m_system.GetKeyModifiers(m_window));
		break;
	case sf::Event::MouseButtonPressed:
		m_context->ProcessMouseButtonDown(event.mouseButton.button,
			m_system.GetKeyModifiers(m_window));
		break;
	case sf::Event::MouseButtonReleased:
		m_context->ProcessMouseButtonUp(event.mouseButton.button,
			m_system.GetKeyModifiers(m_window));
		break;
	case sf::Event::MouseWheelMoved:
		m_context->ProcessMouseWheel(-event.mouseWheel.delta,
			m_system.GetKeyModifiers(m_window));
		break;
	case sf::Event::TextEntered:
		if (event.text.unicode > 32)
			m_context->ProcessTextInput(event.text.unicode);
		break;
	case sf::Event::KeyPressed:
		m_context->ProcessKeyDown(m_system.TranslateKey(event.key.code),
			m_system.GetKeyModifiers(m_window));
		break;
	case sf::Event::KeyReleased:
		if (event.key.code == sf::Keyboard::F8)
		{
			Rocket::Debugger::SetVisible(!Rocket::Debugger::IsVisible());
		};

		if (event.key.code == sf::Keyboard::Escape) {
			m_window->close();
		}

		m_context->ProcessKeyUp(m_system.TranslateKey(event.key.code),
			m_system.GetKeyModifiers(m_window));
		break;
	case sf::Event::Closed:
		m_window->close();
		break;
	default:
	
		break;
	};
	m_context->Update();
}

void Manager::Render()
{
	m_context->Render();
}
