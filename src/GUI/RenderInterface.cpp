/*
 * This source file is part of libRocket, the HTML/CSS Interface Middleware
 *
 * For the latest information, see http://www.librocket.com
 *
 * Copyright (c) 2008-2010 Nuno Silva
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
#include "../Graphics/flextGL.h"
#include <Rocket/Core/Core.h>
#include "RenderInterface.hpp"
#include "../Util/Logger.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>

using namespace GUI;

class RocketSFMLRendererGeometryHandler
{
public:
	GLuint VertexID, IndexID;
	int NumVertices;
	Rocket::Core::TextureHandle Texture;

	RocketSFMLRendererGeometryHandler() : VertexID(0), IndexID(0), Texture(0), NumVertices(0)
	{
	};

	~RocketSFMLRendererGeometryHandler()
	{
		if (VertexID)
			glDeleteBuffers(1, &VertexID);

		if (IndexID)
			glDeleteBuffers(1, &IndexID);

		VertexID = IndexID = 0;
	};
};

struct RocketSFMLRendererVertex
{
	glm::vec2 Position, TexCoord;
	glm::vec4 Color;
};

RocketSFMLRenderer::RocketSFMLRenderer()
{	 
	m_shader.LoadFromFile(Graphics::Shader::VERTEX_SHADER, "gui.vert");
	m_shader.LoadFromFile(Graphics::Shader::FRAGMENT_SHADER, "gui.frag");
	m_shader.AddAttribute("pos");
	m_shader.AddAttribute("UV");
	m_shader.AddAttribute("col");
	m_shader.Link();
	m_shader.AddUniform("translation");
	m_shader.AddUniform("ortho");
	m_shader.AddUniform("textureSampler");
}

RocketSFMLRenderer::~RocketSFMLRenderer()
{
	
	
}

void RocketSFMLRenderer::SetWindow(sf::RenderWindow *window)
{
	m_window = window;

	Resize();
};

sf::RenderWindow *RocketSFMLRenderer::GetWindow()
{
	return m_window;
};

void RocketSFMLRenderer::Resize()
{
	m_ortho = glm::ortho(0.0f, (float)m_window->getSize().x, (float)m_window->getSize().y, 
										0.0f);
	glMatrixMode(GL_MODELVIEW);
	glViewport(0, 0, m_window->getSize().x, m_window->getSize().y);
};

// Called by Rocket when it wants to render geometry that it does not wish to optimise.
void RocketSFMLRenderer::RenderGeometry(Rocket::Core::Vertex* vertices, int num_vertices, int* indices, int num_indices, const Rocket::Core::TextureHandle texture, const Rocket::Core::Vector2f& translation)
{
	Rocket::Core::CompiledGeometryHandle gh = CompileGeometry(vertices,
		num_vertices, indices, num_indices, texture);
	RenderCompiledGeometry(gh, translation);
	ReleaseCompiledGeometry(gh);
}

// Called by Rocket when it wants to compile geometry it believes will be static for the forseeable future.		
Rocket::Core::CompiledGeometryHandle RocketSFMLRenderer::CompileGeometry(Rocket::Core::Vertex* vertices, int num_vertices, int* indices, int num_indices, const Rocket::Core::TextureHandle texture)
{

	std::vector<RocketSFMLRendererVertex> Data(num_vertices);

	for (unsigned long i = 0; i < Data.size(); i++)
	{
		Data[i].Position = *(glm::vec2*)&vertices[i].position;
		Data[i].TexCoord = *(glm::vec2*)&vertices[i].tex_coord;
		Data[i].Color = glm::vec4((float)vertices[i].colour.red/255.f,
									(float)vertices[i].colour.green/255.f,
									(float)vertices[i].colour.blue/255.f, 
									(float)vertices[i].colour.alpha/255.f);
	};
	
	RocketSFMLRendererGeometryHandler *Geometry = new RocketSFMLRendererGeometryHandler();
	Geometry->NumVertices = num_indices;
	
	glGenBuffers(1, &Geometry->VertexID);
	glBindBuffer(GL_ARRAY_BUFFER, Geometry->VertexID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(RocketSFMLRendererVertex) * num_vertices, &Data[0],
		GL_STATIC_DRAW);

	glGenBuffers(1, &Geometry->IndexID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Geometry->IndexID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int) * num_indices, indices, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	Geometry->Texture = texture;

	return (Rocket::Core::CompiledGeometryHandle)Geometry;
}

// Called by Rocket when it wants to render application-compiled geometry.		
void RocketSFMLRenderer::RenderCompiledGeometry(Rocket::Core::CompiledGeometryHandle geometry, const Rocket::Core::Vector2f& translation)
{
	m_shader.Use();
	RocketSFMLRendererGeometryHandler *RealGeometry = (RocketSFMLRendererGeometryHandler *)geometry;
	
	glBindBuffer(GL_ARRAY_BUFFER, RealGeometry->VertexID);
	
	// position
	glEnableVertexAttribArray(m_shader.Attribute("pos"));
	glVertexAttribPointer(m_shader.Attribute("pos"), 2, GL_FLOAT,
		GL_FALSE, sizeof(RocketSFMLRendererVertex), (void*)offsetof(RocketSFMLRendererVertex, Position));
	
	// UV
	glEnableVertexAttribArray(m_shader.Attribute("UV"));
	glVertexAttribPointer(m_shader.Attribute("UV"), 2, GL_FLOAT,
		GL_FALSE, sizeof(RocketSFMLRendererVertex), (void*)offsetof(RocketSFMLRendererVertex, TexCoord));
	
	// Colors
	glEnableVertexAttribArray(m_shader.Attribute("col"));
	glVertexAttribPointer(m_shader.Attribute("col"), 4, GL_FLOAT,
		GL_FALSE, sizeof(RocketSFMLRendererVertex), (void*)offsetof(RocketSFMLRendererVertex, Color));
	
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, RealGeometry->IndexID);
	
	sf::Texture *texture = (sf::Texture *)RealGeometry->Texture;
	if (texture)
		sf::Texture::bind(texture);
	else
		glBindTexture(GL_TEXTURE_2D, 0);
	
	
	// texture sampler
	glUniform1i(m_shader.Uniform("textureSampler"), 0);
	// translation
	glUniform2f(m_shader.Uniform("translation"), translation.x, translation.y);
	// ortho
	glUniformMatrix4fv(m_shader.Uniform("ortho"), 1, GL_FALSE, glm::value_ptr(m_ortho));
	
	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	glDrawElements(GL_TRIANGLES, RealGeometry->NumVertices, GL_UNSIGNED_INT, nullptr);
		
	glDisable (GL_BLEND);
	
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDisableVertexAttribArray(m_shader.Attribute("pos"));
	glDisableVertexAttribArray(m_shader.Attribute("UV"));
	glDisableVertexAttribArray(m_shader.Attribute("col"));
	m_shader.UnUse();
}


// Called by Rocket when it wants to release application-compiled geometry.		
void RocketSFMLRenderer::ReleaseCompiledGeometry(Rocket::Core::CompiledGeometryHandle geometry)
{
	delete (RocketSFMLRendererGeometryHandler *)geometry;
}

// Called by Rocket when it wants to enable or disable scissoring to clip content.		
void RocketSFMLRenderer::EnableScissorRegion(bool enable)
{
	if (enable)
		glEnable(GL_SCISSOR_TEST);
	else
		glDisable(GL_SCISSOR_TEST);
}

// Called by Rocket when it wants to change the scissor region.		
void RocketSFMLRenderer::SetScissorRegion(int x, int y, int width, int height)
{
	glScissor(x, m_window->getSize().y - (y + height), width, height);
}

// Called by Rocket when a texture is required by the library.		
bool RocketSFMLRenderer::LoadTexture(Rocket::Core::TextureHandle& texture_handle, Rocket::Core::Vector2i& texture_dimensions, const Rocket::Core::String& source)
{
	Rocket::Core::FileInterface* file_interface = Rocket::Core::GetFileInterface();
	Rocket::Core::FileHandle file_handle = file_interface->Open(source);
	if (file_handle == NULL)
		return false;

	file_interface->Seek(file_handle, 0, SEEK_END);
	size_t buffer_size = file_interface->Tell(file_handle);
	file_interface->Seek(file_handle, 0, SEEK_SET);

	char* buffer = new char[buffer_size];
	file_interface->Read(buffer, buffer_size, file_handle);
	file_interface->Close(file_handle);

	sf::Texture *texture = new sf::Texture();

	if (!texture->loadFromMemory(buffer, buffer_size))
	{
		delete[] buffer;
		delete texture;

		return false;
	};

	delete[] buffer;

	texture_handle = (Rocket::Core::TextureHandle) texture;
	texture_dimensions = Rocket::Core::Vector2i(texture->getSize().x, texture->getSize().y);

	return true;
}

// Called by Rocket when a texture is required to be built from an internally-generated sequence of pixels.
bool RocketSFMLRenderer::GenerateTexture(Rocket::Core::TextureHandle& texture_handle, const Rocket::Core::byte* source, const Rocket::Core::Vector2i& source_dimensions)
{
	sf::Texture *texture = new sf::Texture();

	if (!texture->create(source_dimensions.x, source_dimensions.y)) {
		delete texture;
		return false;
	}

	texture->update(source, source_dimensions.x, source_dimensions.y, 0, 0);
	texture_handle = (Rocket::Core::TextureHandle)texture;

	return true;
}

// Called by Rocket when a loaded texture is no longer required.		
void RocketSFMLRenderer::ReleaseTexture(Rocket::Core::TextureHandle texture_handle)
{
	delete (sf::Texture *)texture_handle;
}
