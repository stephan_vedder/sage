// 2015 (C) SAGE Engine
// Stephan Vedder
// Patryk Szczygło

#pragma once
#include <Rocket/Core.h>
#include <string>
#include "RenderInterface.hpp"
#include "SystemInterface.hpp"
#include "../Script/ScriptMngr.hpp"

namespace GUI
{
	class Manager
	{
	public: 
		Manager(sf::RenderWindow&);
		~Manager();

	public:
		Rocket::Core::ElementDocument* LoadDocument(const std::string& filename, Script::Manager* script);

		Rocket::Core::Context& getContext() { return *m_context; }
		sf::RenderWindow& getWindow() { return *m_window; }

		void Update(sf::Event& event);
		void Render();
	private:
		RocketSFMLRenderer m_renderer;
		RocketSFMLSystemInterface m_system;
		Rocket::Core::Context* m_context;
		sf::RenderWindow* m_window;

		std::vector<Rocket::Core::ElementDocument*> m_documents;
	};
}
