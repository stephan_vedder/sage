// 2014 (C) SAGE Engine
// Stephan Vedder
#include "Map.hpp"
#include "Util/Logger.hpp"
#include "Util/ListDirectory.hpp"
#include "Util/tinyxml2.h"
#include "AssetsManager/AssetsManager.hpp"
#include <memory>
#include <SFML/Graphics.hpp>
#include "Config.hpp"
using namespace tinyxml2;
using namespace Graphics;

Map::Map() : m_size(0, 0)
{
}

Map::~Map()
{

}

void Map::Load(const std::string& filename)
{
	XMLDocument doc;
	int error = doc.LoadFile((Config::MAP_DIR+filename).c_str());

	if (error != XML_NO_ERROR)
	{
		SAGELogCrash("TinyXML Error: " + std::string(doc.GetErrorStr1()));
	}

	auto root = doc.FirstChildElement("map");
	if (root == nullptr)
	{
		SAGELogCrash("Map: " + filename+ "\nMissing root element (map)");
	}

	auto mapname = root->Attribute("name");
	if (mapname == nullptr)
	{
		m_name = "Unnamed";
	}
	else
	{
		m_name = mapname;
	}


	auto heightmap = root->FirstChildElement("heightmap");
	if (heightmap == nullptr)
	{
		SAGELogCrash("Map: " + filename + "\nMissing heightmap");
	}
	else
	{
		auto tex = Assets::Manager::loadImage("GFX_TERRAIN_HEIGHTMAP", heightmap->GetText());
		m_terrain.SetHeightmap(tex);
	}


	auto size = root->FirstChildElement("size");
	if (size)
	{
		m_size.x = size->IntAttribute("x");
		m_size.y = size->IntAttribute("y");
	}
}

void Map::Create()
{
	m_terrain.Create(m_size);
}

void Map::Update()
{

}

std::vector<Map> Map::GetMaps()
{
	std::vector<Map> maps;
	std::vector<std::string> files;
	Util::Directory::getFiles(files, Config::MAP_DIR);

	for (const auto& file : files)
	{
		Map m;
		std::size_t dot = file.find_last_of(".");
		if (dot != std::string::npos)
		{
			std::string ext = file.substr(dot, file.size() - dot);
			if(ext == ".map")
			{
				m.Load(file);
				maps.push_back(m);
			}
		}
	}

	return maps;
}
