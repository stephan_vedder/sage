// 2014 (C) SAGE Engine
// Stephan Vedder
#include "Config.hpp"

//Default values
uint16_t Config::WIDTH  = 800;
uint16_t Config::HEIGHT = 600;

uint8_t Config::GL_MAJOR = 3;
uint8_t Config::GL_MINOR = 3;

uint8_t Config::COLOR       = 32;
uint8_t Config::DEPTH       = 24;
uint8_t Config::STENCIL     = 8;
uint8_t Config::ALIASING    = 4;

std::string Config::GAME_NAME = "SAGE";
std::string Config::GUI_DIR = "./data/gui/";
std::string Config::SHADER_DIR = "./shader/";
std::string Config::MAP_DIR = "./maps/";
