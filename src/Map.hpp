// 2014 (C) SAGE Engine
// Stephan Vedder
#pragma once
#include <string>
#include <glm/glm.hpp>
#include <vector>
#include "Graphics/Terrain.hpp"

namespace Graphics
{
	class Map
	{
	public:
		Map();
		~Map();

		void Load(const std::string& filename);
		void Create();
		void Update();

		inline Graphics::Terrain& GetTerrain()
		{
			return m_terrain;
		}

		inline const std::string& GetName()
		{
			return m_name;
		}

		static std::vector<Map> GetMaps();

	private:
		glm::ivec2 m_size;
		Graphics::Terrain m_terrain;
		std::string m_name;
	};
}
