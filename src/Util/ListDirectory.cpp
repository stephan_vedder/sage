#include "ListDirectory.hpp"

#include <SFML/System.hpp>

#ifdef SFML_SYSTEM_WINDOWS
	#include <windows.h>
#else
	#include <dirent.h>
#endif

using namespace Util;

void Directory::getFiles(std::vector<std::string> &out, const std::string &directory)
{
#ifdef SFML_SYSTEM_WINDOWS
	WIN32_FIND_DATA fdFile;
	HANDLE hFind = NULL;

	char sPath[2048];

	//Specify a file mask. *.* = We want everything!
	sprintf(sPath, "%s\\*.*", directory.c_str());

	if ((hFind = FindFirstFile(sPath, &fdFile)) == INVALID_HANDLE_VALUE)
	{
		return;
	}

	do
	{
		if (strcmp(fdFile.cFileName, ".") != 0
			&& strcmp(fdFile.cFileName, "..") != 0)
		{
			//Is is a file
			if (!(fdFile.dwFileAttributes &FILE_ATTRIBUTE_DIRECTORY))
			{
				out.push_back(fdFile.cFileName);
			}
		}
	}
	while (FindNextFile(hFind, &fdFile)); //Find the next file.
#else

	DIR *dpdf;
	struct dirent *epdf;

	dpdf = opendir(directory.c_str());
	if (dpdf != NULL)
	{
		while (epdf = readdir(dpdf))
		{
			if(std::string(epdf->d_name) == "." || std::string(epdf->d_name) == "..")
				continue;
			out.push_back(std::string(epdf->d_name));
			// std::out << epdf->d_name << std::endl;
		}
	}
	closedir(dpdf);
#endif
}
