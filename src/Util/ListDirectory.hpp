// 2015 (C) SAGE Engine
// Patryk Szczygło
#pragma once

#include <string>
#include <vector>

namespace Util
{
	class Directory
	{
	public:
		static void getFiles(std::vector<std::string> &out, const std::string &directory);
	};
}
