// 2014 (C) SAGE Engine
// Stephan Vedder
#pragma once 
#include "../Graphics/flextGL.h"
#include <string>
#include <stdint.h>
#include <exception>
#include <stdexcept>
#include <iostream>

#define SAGELogCrash(message) Util::Logger::Crash(message,__FUNCTION__,__FILE__,__LINE__)

namespace Util
{
	void APIENTRY GLDebug(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length,const GLchar *message,const void *userParam);

	class Logger
	{
	public:
		static inline void Crash(const std::string& msg, const std::string& func,
			const std::string& file, const uint32_t line)
		{
			std::cout <<"CRASH: Crashed in " << file << " line " << std::to_string(line) <<
				"\nIn function: " << func << "\nError message: " << msg << std::endl;
			throw std::runtime_error("CRASH: Crashed in " + file + " line " + std::to_string(line) +
				"\nIn function: " + func + "\nError message: " + msg);
		}	
	};
}
