// 2015 (C) SAGE Engine
// Patryk Szczygło
// Stephan Vedder

#pragma once

#include <chrono>
#include <deque>

namespace Util
{
	class FPS
	{
	public:
		static double getFPS();
		static void update();
		
	private:
		static std::chrono::high_resolution_clock::time_point m_start;
		static std::chrono::high_resolution_clock::time_point m_end;
		static double m_fps; 
		static std::deque<double> m_fps_list;
	};
}
