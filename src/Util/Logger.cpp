// 2014 (C) SAGE Engine
// Stephan Vedder
#include "Logger.hpp"

using namespace Util;

void APIENTRY Util::GLDebug(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar * message,const void * userParam)
{
	std::cout << "OPENGL - DEBUG" << std::endl;
	std::cout << "SEVERITY:" << std::endl;

	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:
		std::cout << "HIGH" << std::endl;
		break;
	case GL_DEBUG_SEVERITY_MEDIUM:
		std::cout << "MEDIUM" << std::endl;
		break;
	case GL_DEBUG_SEVERITY_LOW:
		std::cout << "LOW" << std::endl;
		break;
	case GL_DEBUG_SEVERITY_NOTIFICATION:
		std::cout << "NOTIFICATION" << std::endl;
		break;
	}
	
	
	switch (type) 
	{
	case GL_DEBUG_TYPE_ERROR:
		std::cout << "ERROR" << std::endl;
		SAGELogCrash(message);		
		break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
		std::cout << "DEPRECATED_BEHAVIOR" << std::endl;
		break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
		std::cout << "UNDEFINED_BEHAVIOR" << std::endl;
		break;
	case GL_DEBUG_TYPE_PORTABILITY:
		std::cout << "PORTABILITY" << std::endl;
		break;
	case GL_DEBUG_TYPE_PERFORMANCE:
		std::cout << "PERFORMANCE" << std::endl;
		break;
	case GL_DEBUG_TYPE_OTHER:
		std::cout << "OTHER" << std::endl;
		break;
	}
	
	std::cout << message << std::endl;
}
