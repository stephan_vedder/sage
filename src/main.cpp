// 2014 (C) SAGE Engine
// Stephan Vedder
#include "Game.hpp"
#include <iostream>
int main(int argc, char** argv)
{
	try
	{
		Game g;
		g.Run();
	}
	catch (std::exception e)
	{
		int hold = 0;
		std::cout << e.what() << std::endl;
		std::cin >> hold;
	}
	return 0;
}