// 2014 (C) SAGE Engine
// Stephan Vedder
#pragma once
#include <SFML/Graphics.hpp>
#include <memory>
#include "Map.hpp"
#include "Model/ModelLoader.hpp"

class Graphics::Map;

namespace Graphics {
	class Manager;
}

namespace GUI
{
	class Manager;
}

namespace Script
{
	class Manager;
}

class Game
{
public:
    Game();
    ~Game();

    void Run();

	inline Graphics::Manager* GetGraphics()
	{
		return m_graphics.get();
	}

	inline Script::Manager* GetScript()
	{
		return m_script.get();
	}

	inline GUI::Manager* GetGUI()
	{
		return m_gui.get();
	}

	inline Graphics::Map& getCurrentMap()
	{
		return *m_current_map;
	}

	void loadCurrentMap(const std::string& filepath);
	
private:
	void updateCamera();

    sf::RenderWindow m_window;
    std::unique_ptr<Graphics::Manager>  m_graphics;
	std::unique_ptr<Script::Manager>	m_script;
	std::unique_ptr<GUI::Manager>		m_gui;
	
	std::unique_ptr<Graphics::Map> m_current_map;
};
