// 2015 (C) SAGE Engine
// Patryk Szczygło

#include <string>
#include <vector>
#include <Rocket/Core/Core.h>
#include "RegisteredFunctions.hpp"
#include "../AssetsManager/AssetsManager.hpp"
#include "../Script/ScriptHelper.hpp"
#include "../GUI/EventManager.hpp"
#include "../Util/ListDirectory.hpp"
#include "../Util/FPS.hpp"

// Core

int L_exit(lua_State *L)
{
	Script::Helper::GetGUI()->getWindow().close();
	return 0;
}

int L_listFiles(lua_State *L)
{
	const std::string dir = lua_tostring(L, 1);
	
	std::vector<std::string> files;
	Util::Directory::getFiles(files, dir);
	
    lua_createtable(L, files.size(), 0);
    int l_files = lua_gettop(L);
    int index = 1;
    
    auto iter = files.begin();
    while(iter != files.end()) {
        lua_pushstring(L, (*iter).c_str());
        lua_rawseti(L, l_files, index);
        ++iter;
        ++index;
    }
    return 1;
}

int L_getFPS(lua_State *L)
{
	const double fps = Util::FPS::getFPS();
	lua_pushnumber(L, fps);
	return 1;
}

// Assets

int L_playSound(lua_State *L)
{
	const std::string id = lua_tostring(L, 1);
	const std::string filepath = lua_tostring(L, 2);
	Assets::Manager::loadSound(id ,filepath)->play();
	
	return 0;
}

// Documents

int L_loadDocument(lua_State *L)
{
	const std::string filepath = lua_tostring(L, 1);
	
	Script::Helper::GetGUI()->LoadDocument(filepath.c_str(), Script::Helper::GetScript());
	
	return 0;
}

int L_showDocument(lua_State *L)
{
	const std::string id = lua_tostring(L, 1);
	
	auto doc = Script::Helper::GetGUI()->getContext().GetDocument(id.c_str());
	if(doc)
		doc->Show();
	
	return 0;
	
}

int L_hideDocument(lua_State *L)
{
	const std::string id = lua_tostring(L, 1);
	
	auto doc = Script::Helper::GetGUI()->getContext().GetDocument(id.c_str());
	if(doc)
		doc->Hide();
	
	return 0;	
}

int L_registerEventToElement(lua_State *L)
{
	const std::string id_doc = lua_tostring(L, 1);
	const std::string element_name = lua_tostring(L, 2);
	const std::string event_name = lua_tostring(L, 3);
	const std::string func_name = lua_tostring(L, 4);
	
	auto doc = Script::Helper::GetGUI()->getContext().GetRootElement()->GetElementById(id_doc.c_str());
	if(doc)
	{
		auto elem = doc->GetElementById(element_name.c_str());
		if(elem)
			GUI::EventManager::registerEvent(elem, event_name.c_str(), func_name.c_str());
	}
	
	return 0;	
}

int L_setInnerRML(lua_State *L)
{
	const std::string element_name = lua_tostring(L, 1);
	const std::string content = lua_tostring(L, 2);
	
	auto elem = Script::Helper::GetGUI()->getContext().GetRootElement()->GetElementById(element_name.c_str());
	if(elem)
	{
		elem->SetInnerRML(content.c_str());
	}
	return 0;
}

int L_addElement(lua_State *L)
{
	const std::string doc_id = lua_tostring(L, 1);
	const std::string element_name = lua_tostring(L, 2);
	const std::string content = lua_tostring(L, 3);
	
	auto doc = Script::Helper::GetGUI()->getContext().GetDocument(doc_id.c_str());
	if(doc)
	{
		Rocket::Core::Element* new_element = doc->CreateElement("div");
		if(new_element)
		{
			new_element->SetId(element_name.c_str());
			new_element->SetInnerRML(content.c_str());
			doc->AppendChild(new_element);
		}
		new_element->RemoveReference();
	}
	return 0;
}

int L_addChild(lua_State *L)
{
	const std::string doc_id = lua_tostring(L, 1);
	const std::string parent_id = lua_tostring(L, 2);
	const std::string element_name = lua_tostring(L, 3);
	const std::string content = lua_tostring(L, 4);
	
	auto doc = Script::Helper::GetGUI()->getContext().GetDocument(doc_id.c_str());
	if(doc)
	{
		Rocket::Core::Element* new_element = doc->CreateElement("div");
		if(new_element)
		{
			new_element->SetId(element_name.c_str());
			new_element->SetInnerRML(content.c_str());
			
			auto parent_elem = doc->GetElementById(parent_id.c_str());
			if(parent_elem)
				parent_elem->AppendChild(new_element);
		}
		new_element->RemoveReference();
	}
	return 0;
}

int L_loadMap(lua_State *L)
{
	const std::string filepath = lua_tostring(L, 1);

	Script::Helper::GetGame()->loadCurrentMap(filepath);

	return 0;
}


