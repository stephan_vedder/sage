// 2014 (C) SAGE Engine
// Stephan Vedder
#pragma once 
#include <lua.hpp>

namespace Script
{
	class Manager
	{
	public:
		Manager();
		~Manager();
		
		void runScript(const std::string& filepath);
		void runFunction(const std::string& function_name);

		lua_State* getLuaState();
	private:
		lua_State* m_state;
	};
}
