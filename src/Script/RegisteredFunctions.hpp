// 2015 (C) SAGE Engine
// Patryk Szczygło

#pragma once 
#include <lua.hpp>

// Core
int L_exit(lua_State *L);
int L_listFiles(lua_State *L);
int L_getFPS(lua_State *L);

// Assets
int L_playSound(lua_State *L);


// Documents
int L_loadDocument(lua_State *L);
int L_showDocument(lua_State *L);
int L_hideDocument(lua_State *L);
int L_registerEventToElement(lua_State *L);
int L_setInnerRML(lua_State *L);
int L_addElement(lua_State *L);
int L_addChild(lua_State *L);

// Game
int L_loadMap(lua_State *L);
