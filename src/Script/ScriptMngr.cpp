// 2014 (C) SAGE Engine
// Stephan Vedder
#include "../Util/Logger.hpp"

#include "ScriptMngr.hpp"
#include "RegisteredFunctions.hpp"
using namespace Script;

Manager::Manager() : m_state(nullptr)
{
	m_state = luaL_newstate();
	if (m_state == nullptr)
		SAGELogCrash("Failed to initialize Lua-JIT!");

	luaL_openlibs(m_state);
	
	luaL_dostring(m_state,"Game = Game or {}");
	lua_getglobal(m_state,"Game");
	int game = lua_gettop(m_state);

	if(lua_isnil(m_state, game))
		SAGELogCrash("Error creating lua table in C++: failed to create \"Game\"");
	
	// Core
	lua_pushcfunction(m_state, L_exit);
	lua_setfield(m_state, game, "exit");
	
	lua_pushcfunction(m_state, L_getFPS);
	lua_setfield(m_state, game, "getFPS");
	
	lua_pushcfunction(m_state, L_listFiles);
	lua_setfield(m_state, game, "listFiles");
	
	// Assets
	lua_pushcfunction(m_state, L_playSound);
	lua_setfield(m_state, game, "playSound");

	// Documents
	lua_pushcfunction(m_state, L_loadDocument);
	lua_setfield(m_state, game, "loadDocument");
	
	lua_pushcfunction(m_state, L_showDocument);
	lua_setfield(m_state, game, "showDocument");
	
	lua_pushcfunction(m_state, L_hideDocument);
	lua_setfield(m_state, game, "hideDocument");
	
	lua_pushcfunction(m_state, L_registerEventToElement);
	lua_setfield(m_state, game, "registerEventToElement");
	
	lua_pushcfunction(m_state, L_setInnerRML);
	lua_setfield(m_state, game, "setInnerRML");
	
	lua_pushcfunction(m_state, L_addElement);
	lua_setfield(m_state, game, "addElement");
	
	lua_pushcfunction(m_state, L_addChild);
	lua_setfield(m_state, game, "addChild");

	lua_pushstring(m_state, "");
	lua_setfield(m_state, game, "clicked_element_id");

	// Game
	lua_pushcfunction(m_state, L_loadMap);
	lua_setfield(m_state, game, "loadMap");

	lua_pop(m_state, 1); // pop game
}

Manager::~Manager()
{
	if (m_state)
		lua_close(m_state);
}

void Manager::runScript(const std::string& filepath)
{
	luaL_dofile(m_state, filepath.c_str());
}

void Manager::runFunction(const std::string& function_name)
{
	lua_getglobal(m_state, function_name.c_str());
	lua_pcall(m_state, 0, 0, 0);
}
		
lua_State* Manager::getLuaState()
{
	return m_state;
}
