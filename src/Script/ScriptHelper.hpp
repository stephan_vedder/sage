// 2015 (C) SAGE Engine
// Patryk Szczygło

#pragma once

#include "../Game.hpp"
#include "../GUI/GuiManager.hpp"
#include "ScriptMngr.hpp"

namespace Script
{
	class Helper
	{
	public:
		static void init(Game* game);
		static inline Game* GetGame()
		{
			return m_game;
		}

		static inline Script::Manager* GetScript()
		{
			return m_game->GetScript();
		}

		static inline GUI::Manager* GetGUI()
		{
			return m_game->GetGUI();
		}
	private:
		static Game* m_game;
	};
}
